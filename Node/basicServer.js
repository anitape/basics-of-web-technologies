var express = require('express');
var app = express();
let bodyParser = require('body-parser');

// Create application/x-www-form-urlencoded parser
let urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); // for reading JSON

var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: "example_db"
});

app.get('/', function (req, res) {
    //res.send('Hello World!');
    con.connect(function(err) {
        if (err) throw err;
        var sql = "SELECT event_date.Date, event.Name, event.Type, Location.Location_name"
        + " FROM event_date, event, location"
        + " WHERE event_date.Event_id = event.Event_id and event.Location_Location_id = Location.Location_id"
        + " and event_date.Date >= '2020-05-01' and event_date.Date <= '2020-05-31'"
        + " GROUP BY Name"
        + " ORDER BY event_date.Date";
        con.query(sql, function (err, result, fields) {
            if (err) throw err;
            res.send(result); // näkyykö selaimessa?
            console.log(result);
        });
    });
});

app.post("/api/event", urlencodedParser, function (req, res) {
    console.log("Got a POST request ");

    //console.log("Request body: " + req.body);
    //console.log("Request body length: " + req.body.getLength);

    console.log("body: %j", req.body);
    // get JSON-object from the http-body
    var jsonObj = req.body;
    console.log("Arvo: " + jsonObj.eventName);
    // make updates to the database
    res.send(req.body);
});

var server = app.listen(8080, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log("Example app listening at http://%s:%s", host, port)
});



