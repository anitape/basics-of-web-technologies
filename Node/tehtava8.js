var express = require('express');
var app = express();

var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "anita910225",
    database: "example_db"
});

con.connect(function(err){
    if(err) throw err;
    console.log('Connected to MySQL!');
});

app.get("/api/location", function (req, res) {
    var q = require('url').parse(req.url, true).query;
    var locationName = q.name;
    var alteredResult;
    var string;
    getLocationResult(locationName,function (err, result) {
        if (err) {
            console.log("Database error!");
            throw err;
        }
        else {
            string = JSON.stringify(result);
            alteredResult = '{"numOfRows":' +result.length+', "rows": '+string+'}';
            console.log("Altered result: " + alteredResult);
            res.send(alteredResult);
        }

    })
});

var getLocationResult = function (locationName, callback) {
    var sql = "SELECT Location_id, Location_name, Street_address, City, Zip, Country"
        + " FROM location WHERE Location_name = ?";
    console.log(sql);
    con.query(sql, [locationName], function (err, result) {
        if (err) return callback(err);
        console.log(result); // näkyykö selaimessa?
        return callback(null, result);
    });
};

var server = app.listen(8080, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port)
});