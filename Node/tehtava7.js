var express = require('express');
var app = express();
var cors = require('cors');

app.use(cors());

var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "anita910225",
    database: "example_db"
});

con.connect(function(err){
    if(err) throw err;
    console.log('Connected to MySQL!');
});

app.get('/api/events', function (req, res) {
    console.log("Get events from a certain period.");
    var q  = require('url').parse(req.url, true).query;
    var params = q.start + " " + q.end;
    var startDate  = q.start;
    var endDate = q.end;
    var alteredResult;
    var string;
    console.log("Got the params: " + params);
    console.log(startDate);
    console.log(endDate);
    //res.send(params);

    getResult(startDate, endDate, function(err, result) {
        if (err) {console.log("Database error!"); throw err;
    } else {
        console.log("JSON.stringify: " + JSON.stringify(req.headers));
        console.log("Result: " + result);
        string = JSON.stringify(result);
        alteredResult = '{"numOfRows":' +result.length+', "rows": '+string+'}';
        console.log("Altered result: " + alteredResult);
        res.send(alteredResult);
    }
    });
});

var getResult = function(startDate, endDate, callback) {
        var sql = "SELECT event_date.Date, event.Name, event.Type, Location.Location_name"
            + " FROM event_date, event, location"
            + " WHERE event_date.Event_id = event.Event_id and event.Location_Location_id = Location.Location_id"
            + " and event_date.Date >= ? and event_date.Date <= ?"
            + " GROUP BY Name"
            + " ORDER BY event_date.Date";
        console.log(sql);
        con.query(sql, [startDate, endDate], function (err, result) {
            if (err) return callback(err);
            console.log(result); // näkyykö selaimessa?
            return callback(null, result);
        });
    };

var server = app.listen(8080, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log("Example app listening at http://%s:%s", host, port)
});