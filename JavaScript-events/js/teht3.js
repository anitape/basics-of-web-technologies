function add() {
    var luku1 = parseInt(document.querySelector("#num1").value);
    var luku2 = parseInt(document.querySelector("#num2").value);
    var calculate = luku1 + luku2;
    document.querySelector('#vastaus').innerHTML = calculate;
}

function subtract() {
    var luku1 = parseInt(document.querySelector("#num1").value);
    var luku2 = parseInt(document.querySelector("#num2").value);
    var calculate = luku1 - luku2;
    document.querySelector('#vastaus').innerHTML = calculate;
}

function multiply() {
    var luku1 = parseInt(document.querySelector("#num1").value);
    var luku2 = parseInt(document.querySelector("#num2").value);
    var calculate = luku1 * luku2;
    document.querySelector('#vastaus').innerHTML = calculate;
}

function divide() {
    var luku1 = parseInt(document.querySelector("#num1").value);
    var luku2 = parseInt(document.querySelector("#num2").value);
    var calculate = luku1 / luku2;
    document.querySelector('#vastaus').innerHTML = calculate;
}
