const pics = [
  {
    thumb: 'http://www.fillmurray.com/100/100',
    big: 'http://www.fillmurray.com/640/480',
  },
  {
    thumb: 'http://lorempixel.com/100/100/sports/1/',
    big: 'http://lorempixel.com//640/480/sports/1/',
  },
  {
    thumb: 'https://placeimg.com/100/100/tech',
    big: 'https://placeimg.com/640/480/tech',
  },
];

for (let i = 0; i < pics.length; i++) {
  const unor = document.querySelector('ul');
  const lista = document.createElement('li');
  const image = document.createElement('img');
  image.src = pics[i].thumb;
  lista.appendChild(image);
  unor.appendChild(lista);
}

const images = document.querySelectorAll('img');
for (let j = 0; j < images.length; j++) {
  images[j].addEventListener('click', () => {
    const div = document.querySelector('div');
    div.classList.remove('hidden');
    div.classList.add('visible');
    div.querySelector('img').setAttribute('src', pics[j].big);
  });
}

const div = document.querySelector('div');
const bigimg = div.querySelector('img');
bigimg.addEventListener('click', () => {
  div.classList.remove('visible');
  div.classList.add('hidden');
});


