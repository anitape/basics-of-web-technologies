// etsii oikean kuvan alt-tagin avulla (oikeasti id:n avulla)
const kuva1 = document.querySelector('[alt="kuva 1"]');

//kuva näkyviin, muutan kuvan style-määritystä
kuva1.style = 'display: inline';


//etsii kuvan 2 id:n avulla
//TODO: lisää kuva2:lle id-arvo html-sivulla
const kuva2 = document.getElementById('kuva2');

//poistetaan class-määrityksestä sana hidden
kuva2.classList.remove('hidden');

//lisätään kuva2 class-määritykseen sana 'visible'
kuva2.classList.add('visible');

