/*
	Javascript-tiedosto AJAX-tehtäviä varten.
*/

// Etsitään HTML-sivulta tarvittavat komponentit id:n avulla.
const hakubutton = document.getElementById('hakunappi');
const hakukentta = document.getElementById('hakuteksti');
const tulosalue = document.getElementById('tulos');
// Datan hakuosoitteen alkuosa.
const apiurl = 'http://api.tvmaze.com/search/shows?q=';

// Lisätään nappulalle tapahtumankäsittelijä.
hakubutton.addEventListener('click', tee_haku);

// Idea: tämä fetch-osa säilyy aina lähes vakiona.
function tee_haku()  {
  let hakusana = document.getElementById('hakuteksti').value;
  fetch(apiurl + hakusana).then(function(response) {
    return response.json();
  }).then(function(json) {
      naytaKuva(json);
  }).catch(function(error){       	// Jos tapahtuu virhe,
      console.log(error);           // kirjoitetaan virhe konsoliin.
    });
}

// Idea: tämä json-osa muokataan sovelluksen mukaan
/*function naytaVastaus(json) {
    let html = '';
    html += 'Nyt pitäisi näyttää tuloksia... <br>';
    html += 'Tuloksia ' + json.length + ' kpl. <br>';
    html += 'Ekan sarjan nimi: ' + json[0].show.name + '<br>';
    html += 'Kuvan osoite : ' + json[0].show.image.medium + '<br>';
    tulosalue.innerHTML = html;
return;
} */

function naytaKuva (json) {
tulosalue.innerHTML = 'Tuloksia yhteensä' + json.length + ' kpl. <br><br> '
    for (let i = 0; i < json.length; i++) {
const teksti = document.createElement('p');
const image = document.createElement('img');
tulosalue.appendChild(teksti);
tulosalue.appendChild(image);
teksti.innerHTML = 'Sarjan nimi: ' + json[i].show.name + '<br>' +
    'Sarjan linkki kotisivulle: ' + json[i].show.url + '<br>' +
    'Sarjan genre: ' + json[i].show.genres + '<br>'+
'Sarjan yhteenveto: ' + json[i].show.summary;
const nimi = json[i].show.name;
image.src = json[i].show.image != null ? json[i].show.image.medium : "#";
image.alt = nimi;
    }
}
